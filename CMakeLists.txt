cmake_minimum_required(VERSION 3.5)

# build options

option(LIBHERMES_BUILD_TESTS "Build ${PACKAGE_NAME} tests" OFF)
option(THREADS_PREFER_PTHREAD_FLAG "The -pthread compiler and linker flag preference." TRUE)
option(CMAKE_EXPORT_COMPILE_COMMANDS "If enabled, generates a compile_commands.json file containing the exact compiler calls." ON)

# compile options

set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Specifies the build type. Possible values are empty, Debug, Release, RelWithDebInfo, MinSizeRel, ...")
# CMAKE_<LANG>_FLAGS_<CONFIG> new in cmake version 3.11
set(CMAKE_CXX_FLAGS_RELEASE "-O2 -DNDEBUG")
set(SPDLOG_ACTIVE_LEVEL "SPDLOG_LEVEL_TRACE" CACHE STRING "This will turn on/off logging statements at compile time.")
set(FMT_STRING_ALIAS "1" CACHE STRING "Constructs a compile-time format string.")
set(FMT_HEADER_ONLY "1" CACHE STRING "Use fmt library in the header-only manner.")

# project details

include("cmake/package.cmake")

package(libhermes
    FULL_VERSION "6.0.2")

project(LibHermes
    VERSION ${PACKAGE_VERSION}
    LANGUAGES CXX)

# package dependencies

include("cmake/package_dependencies.cmake")

add_package_dependencies(FROM_FILE "dependencies.txt")

# dependencies

set(CMAKE_FIND_PACKAGE_PREFER_CONFIG TRUE)

find_package(Threads REQUIRED)

find_package(Boost 1.53.0
    COMPONENTS
        date_time
        program_options
        system
    REQUIRED)

# build

function(set_grpc_properties_on_targets)
    foreach(target_name ${ARGN})
        message(STATUS "setting grpc properties on: " ${target_name})
        set_target_properties(${target_name} PROPERTIES
            CXX_STANDARD 14
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO)
        target_compile_options(${target_name}
            PRIVATE
                "-Wno-unused-parameter")
        target_link_libraries(${target_name}
            PUBLIC
                protobuf::libprotobuf)
    endforeach()
endfunction()

function(set_common_properties_on_targets)
    foreach(target_name ${ARGN})
        message(STATUS "setting common properties on: " ${target_name})
        set_target_properties(${target_name} PROPERTIES
            CXX_STANDARD 14
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO
            ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
            LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
        target_include_directories(${target_name}
            PUBLIC
                $<INSTALL_INTERFACE:include>
                $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
                $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
                ${PostgreSQL_INCLUDE_DIRS}
                ${LIBSTRONG_INCLUDE_DIRS}
            PRIVATE
                ${CMAKE_CURRENT_SOURCE_DIR})
        target_compile_options(${target_name}
            PRIVATE
                $<$<CXX_COMPILER_ID:GNU>:-Wall -Wextra -ggdb -grecord-gcc-switches>)
    endforeach()
endfunction()

add_library(fred-messenger-iface STATIC
    ${FRED_MESSENGER_PROTO_SRCS}
    ${FRED_MESSENGER_GRPC_SRCS})

add_library(FredMessengerIface::library ALIAS fred-messenger-iface)

set_grpc_properties_on_targets(
    fred-messenger-iface)

add_library(hermes STATIC
    src/connection.cc
    src/email/email.cc
    src/email/proto_unwrap.cc
    src/email/proto_wrap.cc
    src/email/send.cc
    src/letter/message_data.cc
    src/letter/proto_unwrap.cc
    src/letter/proto_wrap.cc
    src/letter/send.cc
    src/sms/message_data.cc
    src/sms/proto_unwrap.cc
    src/sms/proto_wrap.cc
    src/sms/send.cc
    src/exception.cc
    src/hermes.cc
    src/reference.cc
    src/proto_wrap.cc)

add_library(LibHermes::library ALIAS hermes)

target_link_libraries(hermes
    Threads::Threads
    Boost::program_options
    FredMessengerIface::library
    LibStrong::library
    gRPC::grpc++)

set_common_properties_on_targets(
    hermes)

if(LIBHERMES_BUILD_TESTS)

add_subdirectory(test)

endif()

# installation

find_program(GIT_PROGRAM git)
get_directory_property(HAS_PARENT PARENT_DIRECTORY)
if(NOT HAS_PARENT)

    include(GNUInstallDirs)

    install(TARGETS hermes
        DESTINATION ${CMAKE_INSTALL_SBINDIR})

    add_custom_target(uninstall
        COMMAND xargs -L10 rm -v < ${CMAKE_CURRENT_BINARY_DIR}/install_manifest.txt
        COMMAND cat ${CMAKE_CURRENT_BINARY_DIR}/install_manifest.txt | xargs -L1 dirname | sort | uniq | xargs -L10 rmdir -p --ignore-fail-on-non-empty)

endif()

# dist

include("cmake/package_dist.cmake")
package_dist(PACKAGE_NAME "${PACKAGE_NAME}"
             PACKAGE_PREFIX "${LIBHERMES_PREFIX}"
             PACKAGE_TARNAME "${PACKAGE_TARNAME}")
