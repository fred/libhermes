/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONNECTION_HH_C7C7378373234C06843AFF2A3AFE6DC0
#define CONNECTION_HH_C7C7378373234C06843AFF2A3AFE6DC0

#include "libhermes/service.hh"
#include "libhermes/strong_type.hh"

#include <chrono>
#include <memory>

namespace LibHermes {

constexpr std::chrono::seconds no_explicit_connection_timeout{0};

class RpcStubAlias;

template <typename S>
class Connection
{
public:
    class Accessor;

    using ConnectionString = StrongString<struct ConnectionStringTag_>;

    explicit Connection(const ConnectionString& _connection_string);

    std::chrono::seconds get_timeout() const;

    void set_timeout(std::chrono::seconds _timeout);

    ~Connection();

private:
    struct RpcStubDeleter
    {
        void operator()(RpcStubAlias*) const;
    };

    std::unique_ptr<RpcStubAlias, RpcStubDeleter> rpc_stub_wrapper_;

    std::chrono::seconds timeout_;
};

extern template class Connection<Service::EmailMessenger>;
extern template class Connection<Service::LetterMessenger>;
extern template class Connection<Service::SmsMessenger>;

} // namespace LibHermes

#endif
