/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBHERMES_HH_4866B6E6774947D6A182942165F19F97
#define LIBHERMES_HH_4866B6E6774947D6A182942165F19F97

#include "libhermes/connection.hh"
#include "libhermes/email/send.hh"
#include "libhermes/letter/send.hh"
#include "libhermes/sms/send.hh"

namespace LibHermes {

void libhermes();

} // namespace LibHermes

#endif
