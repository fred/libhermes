/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MESSAGE_DATA_HH_54EB12277E404FC4A7486FFEED70111B
#define MESSAGE_DATA_HH_54EB12277E404FC4A7486FFEED70111B

#include "libhermes/reference.hh"
#include "libhermes/strong_type.hh"
#include "libhermes/struct.hh"

#include <vector>

namespace LibHermes {
namespace Letter {

struct RecipientAddress
{
    using Name = StrongString<struct NameTag_>;
    using Organization = StrongString<struct OrganizationTag_>;
    using Street = StrongString<struct StreetTag_>;
    using City = StrongString<struct CityTag_>;
    using StateOrProvince = StrongString<struct StateOrProvinceTag_>;
    using PostalCode = StrongString<struct PostalCodeTag_>;
    using CountryName = StrongString<struct CountryNameTag_>;

    Name name;
    Organization organization;
    std::vector<Street> street_field;
    City city;
    StateOrProvince state_or_province;
    PostalCode postal_code;
    CountryName country_name;
};

using RecipientUuid = StrongUuid<struct RecipientUuidTag_>;
using Type = StrongString<struct TypeTag_>;
using BodyTemplate = StrongString<struct BodyTemplateTag_>;
using BodyTemplateUuid = StrongUuid<struct BodyTemplateUuidTag_>;
using FileUuid = StrongUuid<struct FileUuidTag_>;

struct MessageData
{
    RecipientAddress recipient_address;
    std::vector<RecipientUuid> recipient_uuids;
    Optional<Type> type;
    BodyTemplate body_template;
    Struct context;
    Optional<BodyTemplateUuid> body_template_uuid;
    Optional<FileUuid> file_uuid;
};

MessageData make_minimal_message(
        RecipientAddress _recipient_address,
        BodyTemplate _body_template);

} // namespace LibHermes::Letter
} // namespace LibHermes

#endif
