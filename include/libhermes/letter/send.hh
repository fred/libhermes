/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEND_HH_5AE85657ADB24BEAB9318F15B82FE958
#define SEND_HH_5AE85657ADB24BEAB9318F15B82FE958

#include "libhermes/connection.hh"
#include "libhermes/letter/message_data.hh"
#include "libhermes/exception.hh"
#include "libhermes/reference.hh"
#include "libhermes/service.hh"
#include "libstrong/type.hh"

#include <set>

namespace LibHermes {
namespace Letter {

using Archive = LibStrong::Type<bool, struct ArchiveTag_>;
using ArchiveRendered = LibStrong::Type<bool, struct ArchiveRenderedTag_>;

using LetterUid = StrongString<struct LetterUidTag_>;

class SendFailed : public LibHermes::GrpcException
{
    using LibHermes::GrpcException::GrpcException;
};

LetterUid send(
        const Connection<Service::LetterMessenger>& _connection,
        const MessageData& _message_data,
        const Archive& _archive,
        const ArchiveRendered& _archive_rendered,
        const std::set<Reference>& _references = {});

} // namespace LibHermes::Letter
} // namespace LibHermes

#endif
