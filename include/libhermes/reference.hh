/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef REFERENCE_HH_1A87BCC62D02404F833DB4F3CFB1FB99
#define REFERENCE_HH_1A87BCC62D02404F833DB4F3CFB1FB99

#include "libhermes/strong_type.hh"

namespace LibHermes {

struct Reference
{
    using Type = StrongString<struct ReferenceTypeTag_>;
    using Value = StrongString<struct ReferenceValueTag_>;

    Type type;
    Value value;

    bool operator<(const Reference& _rhs) const;
};

} // namespace LibHermes

#endif
