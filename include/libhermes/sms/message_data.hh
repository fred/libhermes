/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MESSAGE_DATA_HH_546795A0E0B14D8FB19C3192F7E3F00C
#define MESSAGE_DATA_HH_546795A0E0B14D8FB19C3192F7E3F00C

#include "libhermes/reference.hh"
#include "libhermes/strong_type.hh"
#include "libhermes/struct.hh"

#include <vector>

namespace LibHermes {
namespace Sms {

using RecipientPhoneNumber = StrongString<struct RecipientPhoneNumberTag_>;
using RecipientUuid = StrongUuid<struct RecipientUuidTag_>;
using Type = StrongString<struct TypeTag_>;
using BodyTemplate = StrongString<struct BodyTemplateTag_>;
using BodyTemplateUuid = StrongUuid<struct BodyTemplateUuidTag_>;

struct MessageData
{
    RecipientPhoneNumber recipient_phone_number;
    std::vector<RecipientUuid> recipient_uuids;
    Optional<Type> type;
    BodyTemplate body_template;
    Struct context;
    Optional<BodyTemplateUuid> body_template_uuid;
};

MessageData make_minimal_message(
        RecipientPhoneNumber _recipient_phone_number,
        BodyTemplate _body_template);

} // namespace LibHermes::Sms
} // namespace LibHermes

#endif
