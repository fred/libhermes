/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEND_HH_BEB4D52CF2EF4BD6B6F11A4544CF64F2
#define SEND_HH_BEB4D52CF2EF4BD6B6F11A4544CF64F2

#include "libhermes/connection.hh"
#include "libhermes/sms/message_data.hh"
#include "libhermes/exception.hh"
#include "libhermes/reference.hh"
#include "libhermes/service.hh"
#include "libstrong/type.hh"

#include <set>

namespace LibHermes {
namespace Sms {

using Archive = StrongBool<struct ArchiveTag_>;
using OptionalArchive = Optional<Archive>;

using SmsUid = StrongString<struct SmsUidTag_>;

class SendFailed : public LibHermes::GrpcException
{
    using LibHermes::GrpcException::GrpcException;
};

SmsUid send(
        const Connection<Service::SmsMessenger>& _connection,
        const MessageData& _message_data,
        const OptionalArchive& _archive,
        const std::set<Reference>& _references = {});

} // namespace LibHermes::Sms
} // namespace LibHermes

#endif
