/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EMAIL_HH_18B3E3A858DA4B0BBE0742C3BC48DC64
#define EMAIL_HH_18B3E3A858DA4B0BBE0742C3BC48DC64

#include "libhermes/reference.hh"
#include "libhermes/strong_type.hh"
#include "libhermes/struct.hh"

#include "libstrong/type.hh"

#include <map>
#include <set>
#include <vector>

#include <boost/uuid/uuid.hpp>

namespace LibHermes {
namespace Email {

using Sender = StrongString<struct SenderTag_>;
using RecipientEmail = StrongString<struct RecipientEmailTag_>;
using RecipientUuid = StrongUuid<struct RecipientUuidTag_>;
using ExtraHeaderKey = StrongString<struct ExtraHeaderKeyTag_>;
using ExtraHeaderValue = StrongString<struct ExtraHeaderValueTag_>;
using Type = StrongString<struct TypeTag_>;
using SubjectTemplate = StrongString<struct SubjectTemplateTag_>;
using BodyTemplate = StrongString<struct BodyTemplateTag_>;
using BodyTemplateHtml = StrongString<struct BodyTemplateHtmlTag_>;
using AttachmentUuid = StrongUuid<struct AttachmentUuidTag_>;
using SubjectTemplateUuid = StrongUuid<struct SubjectTemplateUuidTag_>;
using BodyTemplateUuid = StrongUuid<struct BodyTemplateUuidTag_>;
using BodyTemplateHtmlUuid = StrongUuid<struct BodyTemplateHtmlUuidTag_>;

struct Email
{
    Optional<Sender> sender;
    std::map<RecipientEmail, std::set<RecipientUuid>> recipients;
    std::map<ExtraHeaderKey, ExtraHeaderValue> extra_headers;
    Optional<Type> type;
    SubjectTemplate subject_template;
    BodyTemplate body_template;
    Optional<BodyTemplateHtml> body_template_html;
    Struct context;
    std::vector<AttachmentUuid> attachments;
    Optional<SubjectTemplateUuid> subject_template_uuid;
    Optional<BodyTemplateUuid> body_template_uuid;
    Optional<BodyTemplateHtmlUuid> body_template_html_uuid;
};

Email make_minimal_email(
        const std::map<RecipientEmail, std::set<RecipientUuid>>& _recipients,
        const SubjectTemplate& _subject_template,
        const BodyTemplate& _body_template);

} // namespace LibHermes::Email
} // namespace LibHermes

#endif
