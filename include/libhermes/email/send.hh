/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SEND_HH_20CE998E70A247D28332AA4ED280D371
#define SEND_HH_20CE998E70A247D28332AA4ED280D371

#include "libhermes/connection.hh"
#include "libhermes/email/email.hh"
#include "libhermes/exception.hh"
#include "libhermes/reference.hh"
#include "libhermes/service.hh"
#include "libstrong/type.hh"

#include <set>
#include <vector>

namespace LibHermes {
namespace Email {

using Archive = LibStrong::Type<bool, struct ArchiveTag_>;

using EmailUid = StrongString<struct EmailUidTag_>;

struct EmailInfo
{
    EmailUid email_uid;
    RecipientEmail recipient_email;
};

class SendFailed : public LibHermes::GrpcException
{
    using LibHermes::GrpcException::GrpcException;
};

EmailUid send(
        const Connection<Service::EmailMessenger>& _connection,
        const Email& _email,
        const Archive& _archive,
        const std::set<Reference>& _references = {});

std::vector<EmailUid> batch_send(
        const Connection<Service::EmailMessenger>& _connection,
        const Email& _email,
        const Archive& _archive,
        const std::set<Reference>& _references = {});

} // namespace LibHermes::Email
} // namespace LibHermes

#endif
