/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTIONS_HH_4227C317DD6B4C05AAE7AFF3F29513E0
#define EXCEPTIONS_HH_4227C317DD6B4C05AAE7AFF3F29513E0

#include <exception>
#include <string>

namespace LibHermes {

class Exception : public std::exception
{
public:
    explicit Exception(std::string _msg);
    const char* what() const noexcept override;

private:
    const std::string msg_;
};

class GrpcException : public Exception
{
public:
    GrpcException(
            int _grpc_status_error_code,
            std::string _grpc_status_error_message,
            std::string _grpc_message_json);
    int error_code() const;
    const std::string& error_message() const;
    const std::string& grpc_message_json() const;

private:
    int grpc_status_error_code_;
    std::string grpc_status_error_message_;
    std::string grpc_message_json_;
};

} // namespace LibHermes

#endif
