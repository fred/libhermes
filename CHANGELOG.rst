ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

6.0.2 (2023-02-22)
------------------

* Operator less in References reversed (fix)

6.0.1 (2023-02-06)
------------------

* Add missing compilation dependency (threads)

6.0.0 (2022-12-01)
------------------

* Add pdf file ID to letters service instead of template with context
  - necessary until enhanced version of messenger is released
* Add timeout for the request duration

5.1.0 (2022-09-01)
------------------

* Add SMS
* Add letters

5.0.0 (2022-05-19)
------------------

* Adapt to new gRPC API where ReferenceType is not Enum but string
  * library API was changed accordingly

4.0.2 (2022-05-13)
------------------

* Remove dependency on liblog

4.0.1 (2022-05-09)
------------------

* Fixed bug when reference were not created from contact UUID(s)

4.0.0 (2022-05-06)
------------------

* Change interface to force unique references

3.0.0 (2022-04-22)
------------------

* Change interface so recipients are consolidated by email address

2.0.1 (2022-04-13)
------------------

* Add optional UUID to recipient's email address fixed

2.0.0 (2022-04-13)
------------------

* Add batch_send
* Add optional UUID to recipient's email address

1.0.3 (2022-03-03)
------------------

* Update cmake build (api)
* Add gitlab CI

1.0.0 (2021-03-29)
------------------

* Initial ``LibHermes`` implementation
