/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEND_LETTER_REQUEST_HH_9FF0E19EFA024FF08C5478D827C0F3EB
#define SEND_LETTER_REQUEST_HH_9FF0E19EFA024FF08C5478D827C0F3EB

#include "include/libhermes/letter/message_data.hh"
#include "include/libhermes/reference.hh"

#include "libstrong/type.hh"

#include <set>

namespace LibHermes {
namespace Letter {

struct SendLetterRequest
{
    using Archive = LibStrong::Type<bool, struct ArchiveTag_>;
    using ArchiveRendered = LibStrong::Type<bool, struct ArchiveRenderedTag_>;

    MessageData message_data;
    Archive archive;
    ArchiveRendered archive_rendered;
    std::set<Reference> references;
};

} // namespace LibHermes::Letter
} // namespace LibHermes

#endif
