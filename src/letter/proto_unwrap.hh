/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_UNWRAP_HH_562772B2F00C42EBBD95A814E41B844F
#define PROTO_UNWRAP_HH_562772B2F00C42EBBD95A814E41B844F

#include "src/letter/send_letter_reply.hh"

#include "fred_api/messenger/service_letter_grpc.pb.h"

namespace LibHermes {
namespace Letter {

SendLetterReply proto_unwrap(const Fred::Messenger::Api::Letter::SendLetterReply& _src);

} // namespace LibHermes::Letter
} // namespace LibHermes

#endif
