/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libhermes/letter/send.hh"

#include "include/libhermes/connection.hh"
#include "include/libhermes/exception.hh"
#include "include/libhermes/service.hh"
#include "include/libhermes/strong_type.hh"

#include "src/letter/proto_unwrap.hh"
#include "src/letter/proto_wrap.hh"
#include "src/letter/send_letter_reply.hh"
#include "src/letter/send_letter_request.hh"
#include "src/get_stub.hh"

#include "fred_api/messenger/service_letter_grpc.pb.h"

#include <google/protobuf/util/json_util.h>

#include <grpcpp/client_context.h>

#include <boost/uuid/uuid_io.hpp>

#include <iostream>
#include <stdexcept>

namespace LibHermes {
namespace Letter {

namespace Api = Fred::Messenger::Api;

namespace {

std::string serialize(const Api::Letter::SendLetterRequest& _api_send_letter_request)
{
    google::protobuf::util::JsonPrintOptions json_print_options;
    json_print_options.preserve_proto_field_names = true;
    json_print_options.add_whitespace = false;
    std::string json;
    google::protobuf::util::MessageToJsonString(_api_send_letter_request, &json, json_print_options);
    return json;
}

void merge_recipients_uuids_to_references(SendLetterRequest& _send_letter_request)
{
    for (const auto& recipient_uuid : _send_letter_request.message_data.recipient_uuids)
    {
        _send_letter_request.references.insert(Reference{Reference::Type{"contact"}, Reference::Value{boost::uuids::to_string(*recipient_uuid)}});
    }
}

} // namespace LibHermes::Letter::{anonymous}

LetterUid send(
        const Connection<Service::LetterMessenger>& _connection,
        const MessageData& _message_data,
        const Archive& _archive,
        const ArchiveRendered& _archive_rendered,
        const std::set<Reference>& _references)
{
    SendLetterRequest send_letter_request{_message_data, _archive, _archive_rendered, _references};
    merge_recipients_uuids_to_references(send_letter_request);
    Api::Letter::SendLetterRequest api_send_letter_request;
    proto_wrap(send_letter_request, &api_send_letter_request);

    grpc::ClientContext context;
    const auto timeout = _connection.get_timeout();
    if (timeout != no_explicit_connection_timeout)
    {
        const auto deadline =
                std::chrono::system_clock::now() +
                std::chrono::seconds(timeout);
        context.set_deadline(deadline);
    }
    Api::Letter::SendLetterReply api_send_letter_reply;
    const auto status = get_stub(_connection).send(&context, api_send_letter_request, &api_send_letter_reply);

    if (!status.ok())
    {
        throw SendFailed(status.error_code(), status.error_message(), serialize(api_send_letter_request));
    }

    return proto_unwrap(api_send_letter_reply).uid;
}

} // namespace LibHermes::Letter
} // namespace LibHermes
