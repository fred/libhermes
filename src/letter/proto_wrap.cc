/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/letter/proto_wrap.hh"

#include "src/proto_wrap.hh"

#include <boost/uuid/uuid_io.hpp>

namespace LibHermes {
namespace Letter {

namespace {

std::string join(const std::vector<RecipientAddress::Street>& _street_field, const std::string& _delimiter)
{
    std::string result;
    for (const auto& item : _street_field)
    {
        result += (result.empty() ? "" : _delimiter) + *item;
    }
    return result;
}

} // namespace LibHermes::Letter::{anonymous}

namespace Api = Fred::Messenger::Api;

template <>
struct InternalTypeTraits<RecipientAddress>
{
    using ProtoType = Fred::Messenger::Api::Letter::Address;
};

template <>
struct InternalTypeTraits<Type>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<BodyTemplate>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<BodyTemplateUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template <>
struct InternalTypeTraits<FileUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template<>
void proto_wrap(const RecipientAddress& _src, InternalTypeTraits<RecipientAddress>::ProtoType* _dst)
{
    _dst->set_name(*(_src.name));
    _dst->set_organization(*(_src.organization));
    _dst->set_street(join(_src.street_field, "\\n"));
    _dst->set_city(*(_src.city));
    _dst->set_state_or_province(*(_src.state_or_province));
    _dst->set_postal_code(*(_src.postal_code));
    _dst->set_country(*(_src.country_name));

}

template<>
void proto_wrap(const BodyTemplateUuid& _src, InternalTypeTraits<BodyTemplateUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

template<>
void proto_wrap(const FileUuid& _src, InternalTypeTraits<FileUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

template<>
void proto_wrap(const MessageData& _src, InternalTypeTraits<MessageData>::ProtoType* _dst)
{
    proto_wrap(_src.recipient_address, _dst->mutable_recipient());
    if (!is_nullopt(_src.type))
    {
        _dst->set_type(**(_src.type));
    }
    //_dst->set_body_template(*(_src.body_template));
    //proto_wrap(_src.context, _dst->mutable_context());
    //if (!is_nullopt(_src.body_template_uuid))
    //{
    //    proto_wrap(*(_src.body_template_uuid), _dst->mutable_body_template_uuid());
    //}
    if (!is_nullopt(_src.file_uuid))
    {
        proto_wrap(*(_src.file_uuid), _dst->mutable_file());
    }
}

template<>
void proto_wrap(const SendLetterRequest& _src, InternalTypeTraits<SendLetterRequest>::ProtoType* _dst)
{
    proto_wrap(_src.message_data, _dst->mutable_message_data());

    _dst->set_archive(*(_src.archive));

    _dst->mutable_references()->Reserve(_src.references.size());
    for (const auto& ref : _src.references)
    {
        proto_wrap(ref, _dst->add_references());
    }
}

} // namespace LibHermes::Letter
} // namespace LibHermes
