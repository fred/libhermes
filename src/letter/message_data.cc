/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libhermes/letter/message_data.hh"

#include <utility>

namespace LibHermes {
namespace Letter {

MessageData make_minimal_message(
        RecipientAddress _recipient_address,
        BodyTemplate _body_template)
{
    return {
        std::move(_recipient_address),
        {},
        Type::nullopt,
        std::move(_body_template),
        {},
        BodyTemplateUuid::nullopt,
        FileUuid::nullopt
    };
}

} // namespace LibHermes::Letter
} // namespace LibHermes
