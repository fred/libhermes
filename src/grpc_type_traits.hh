/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GRPC_TYPE_TRAITS_HH_23BCAD51A78A41F1BEF3877C52981F44
#define GRPC_TYPE_TRAITS_HH_23BCAD51A78A41F1BEF3877C52981F44

#include "include/libhermes/service.hh"

#include "fred_api/messenger/service_email_grpc.grpc.pb.h"
#include "fred_api/messenger/service_letter_grpc.grpc.pb.h"
#include "fred_api/messenger/service_sms_grpc.grpc.pb.h"

namespace LibHermes {

template <typename>
struct GrpcService;

template <typename Service>
using GrpcServiceType = typename GrpcService<Service>::Type;

template <typename Service>
using GrpcStubType = typename GrpcServiceType<Service>::Stub;

template <>
struct GrpcService<Service::EmailMessenger>
{
    using Type = Fred::Messenger::Api::Email::EmailMessenger;
};

template <>
struct GrpcService<Service::LetterMessenger>
{
    using Type = Fred::Messenger::Api::Letter::LetterMessenger;
};

template <>
struct GrpcService<Service::SmsMessenger>
{
    using Type = Fred::Messenger::Api::Sms::SmsMessenger;
};

} // namespace LibHermes

#endif
