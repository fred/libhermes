/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PROTO_WRAP_HH_8DAA7EB0705B42B791423D3DF54B6731
#define PROTO_WRAP_HH_8DAA7EB0705B42B791423D3DF54B6731

#include "include/libhermes/reference.hh"
#include "include/libhermes/struct.hh"

#include "fred_api/messenger/service_email_grpc.pb.h"

namespace LibHermes {

template <typename>
struct InternalTypeTraits
{
    struct ProtoType;
};

template <>
struct InternalTypeTraits<Struct>
{
    using ProtoType = google::protobuf::Struct;
};

template <>
struct InternalTypeTraits<Reference>
{
    using ProtoType = Fred::Messenger::Api::Reference;
};

template <typename InternalType>
void proto_wrap(const InternalType& _src, typename InternalTypeTraits<InternalType>::ProtoType* _dst);

template<>
void proto_wrap(const Struct& _src, InternalTypeTraits<Struct>::ProtoType* _dst);

template<>
void proto_wrap(const Reference& _src, InternalTypeTraits<Reference>::ProtoType* _dst);

} // namespace LibHermes

#endif
