/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/get_stub.hh"

#include "src/connection_rpc_stub_deleter.hh"

#include <chrono>

namespace LibHermes {

template <typename S>
Connection<S>::Connection(const ConnectionString& _connection_string)
    : rpc_stub_wrapper_{Accessor::wrap_stub(_connection_string)},
      timeout_{no_explicit_connection_timeout}
{
}

template <typename S>
Connection<S>::~Connection() = default;

template <typename S>
std::chrono::seconds Connection<S>::get_timeout() const
{
    return timeout_;
}

template <typename S>
void Connection<S>::set_timeout(std::chrono::seconds _timeout)
{
    timeout_ = _timeout;
}

template class Connection<Service::EmailMessenger>;
template class Connection<Service::LetterMessenger>;
template class Connection<Service::SmsMessenger>;

} // namespace LibHermes
