/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/email/proto_unwrap.hh"

namespace LibHermes {
namespace Email {

namespace {

EmailUid proto_unwrap(const Fred::Messenger::Api::Uid& _src)
{
    return EmailUid{_src.value()};
}

} // namespace LibHermes::Email::{anonymous}

SendEmailReply proto_unwrap(const Fred::Messenger::Api::Email::SendEmailReply& _src)
{
    return SendEmailReply{proto_unwrap(_src.data().uid())};
}

} // namespace LibHermes::Email
} // namespace LibHermes
