/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libhermes/email/email.hh"

#include <map>
#include <set>

namespace LibHermes {
namespace Email {

Email make_minimal_email(
        const std::map<RecipientEmail, std::set<RecipientUuid>>& _recipients,
        const SubjectTemplate& _subject_template,
        const BodyTemplate& _body_template)
{
    return Email{
        {},
        _recipients,
        {},
        Type::nullopt,
        _subject_template,
        _body_template,
        BodyTemplateHtml::nullopt,
        {},
        {},
        SubjectTemplateUuid::nullopt,
        BodyTemplateUuid::nullopt,
        BodyTemplateHtmlUuid::nullopt
    };
}


} // namespace LibHermes::Email
} // namespace LibHermes
