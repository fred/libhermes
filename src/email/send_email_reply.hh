/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SEND_EMAIL_REPLY_HH_D5449D536C824EBD9F929A92BA6B61A4
#define SEND_EMAIL_REPLY_HH_D5449D536C824EBD9F929A92BA6B61A4

#include "include/libhermes/email/send.hh"
#include "include/libhermes/strong_type.hh"

#include <vector>

namespace LibHermes {
namespace Email {

struct SendEmailReply
{
    EmailUid uid;
};

struct MultiSendEmailReply
{
    std::vector<EmailInfo> emails;
};

} // namespace LibHermes::Email
} // namespace LibHermes

#endif
