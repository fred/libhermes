/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/email/proto_wrap.hh"

#include "src/proto_wrap.hh"

#include <boost/uuid/uuid_io.hpp>

namespace LibHermes {
namespace Email {

namespace Api = Fred::Messenger::Api;

template <>
struct InternalTypeTraits<Sender>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<std::map<ExtraHeaderKey, ExtraHeaderValue>>
{
    using ProtoType = google::protobuf::Map<std::string, std::string>;
};

template <>
struct InternalTypeTraits<Type>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<SubjectTemplate>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<BodyTemplate>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<BodyTemplateHtml>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<AttachmentUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template <>
struct InternalTypeTraits<SubjectTemplateUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template <>
struct InternalTypeTraits<BodyTemplateUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template <>
struct InternalTypeTraits<BodyTemplateHtmlUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template<>
void proto_wrap(
        const std::map<ExtraHeaderKey, ExtraHeaderValue>& _src,
        google::protobuf::Map<std::string, std::string>* _dst)
{
    for (const auto& item : _src)
    {
        (*_dst)[*(item.first)] = *(item.second);
    }
}

template<>
void proto_wrap(const AttachmentUuid& _src, InternalTypeTraits<AttachmentUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

template<>
void proto_wrap(const SubjectTemplateUuid& _src, InternalTypeTraits<SubjectTemplateUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

template<>
void proto_wrap(const BodyTemplateUuid& _src, InternalTypeTraits<BodyTemplateUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

template<>
void proto_wrap(const BodyTemplateHtmlUuid& _src, InternalTypeTraits<BodyTemplateHtmlUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

namespace {

std::string join(const std::map<RecipientEmail, std::set<RecipientUuid>>& _recipients)
{
    std::string result;
    for (const auto& recipient : _recipients)
    {
        result += result.empty() ? *recipient.first : "," + *recipient.first;
    }
    return result;
}

} // namespace LibHermes::Email::{anonymous}

template<>
void proto_wrap(const Email& _src, InternalTypeTraits<Email>::ProtoType* _dst)
{
    if (!is_nullopt(_src.sender))
    {
        _dst->set_sender(**(_src.sender));
    }
    _dst->set_recipient(join(_src.recipients));
    proto_wrap(_src.extra_headers, _dst->mutable_extra_headers());
    if (!is_nullopt(_src.type))
    {
        _dst->set_type(**(_src.type));
    }
    _dst->set_subject_template(*(_src.subject_template));
    _dst->set_body_template(*(_src.body_template));
    if (!is_nullopt(_src.body_template_html))
    {
        _dst->set_body_template_html(**(_src.body_template_html));
    }
    proto_wrap(_src.context, _dst->mutable_context());
    _dst->mutable_attachments()->Reserve(_src.attachments.size());
    for (const auto& attachment : _src.attachments)
    {
        _dst->add_attachments()->set_value(boost::uuids::to_string(*attachment));
    }
    if (!is_nullopt(_src.subject_template_uuid))
    {
        proto_wrap(*(_src.subject_template_uuid), _dst->mutable_subject_template_uuid());
    }
    if (!is_nullopt(_src.body_template_uuid))
    {
        proto_wrap(*(_src.body_template_uuid), _dst->mutable_body_template_uuid());
    }
    if (!is_nullopt(_src.body_template_html_uuid))
    {
        proto_wrap(*(_src.body_template_html_uuid), _dst->mutable_body_template_html_uuid());
    }
}

template<>
void proto_wrap(const SendEmailRequest& _src, InternalTypeTraits<SendEmailRequest>::ProtoType* _dst)
{
    proto_wrap(_src.message_data, _dst->mutable_message_data());

    _dst->set_archive(*(_src.archive));

    _dst->mutable_references()->Reserve(_src.references.size());
    for (const auto& ref : _src.references)
    {
        proto_wrap(ref, _dst->add_references());
    }
}

} // namespace LibHermes::Email
} // namespace LibHermes
