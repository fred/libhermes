syntax = "proto3";

import "fred_api/messenger/common_types.proto";
import "google/protobuf/struct.proto";
import "google/protobuf/timestamp.proto";

package Fred.Messenger.Api.Email;

/*
RFC 2822 Message-ID
*/
message MessageId
{
    string value = 1;
}

/*
Email message itself
*/
message Email
{
    // If sender is not defined, default sender is used.
    // Valid value contains one or more email addresses, see https://tools.ietf.org/html/rfc5322#section-3.6.2.
    string sender = 1;
    // Required. Must contain exactly one email address.
    string recipient = 2;
    // Additional email headers, see https://tools.ietf.org/html/rfc5322#section-2.2.
    map<string, string> extra_headers = 3;
    // Message type - optional descriptive parameter.
    string type = 12;
    // Name of the templates for subject and body in text and HTML versions.
    // Subject and body templates are required.
    string subject_template = 4;
    string body_template = 5;
    string body_template_html = 6;
    // A context used to render message parts from templates.
    google.protobuf.Struct context = 7;
    // List of attachment UUIDs
    repeated Uuid attachments = 8;
    // UUID of the templates for subject and body in text and HTML versions.
    Uuid subject_template_uuid = 9;
    Uuid body_template_uuid = 10;
    Uuid body_template_html_uuid = 11;
}

/*
Complete email message and its metadata
*/
message EmailEnvelope
{
    // Email message unique identifier
    Uid uid = 1;
    google.protobuf.Timestamp create_datetime = 2;
    google.protobuf.Timestamp send_datetime = 3;
    google.protobuf.Timestamp delivery_datetime = 4;
    int32 attempts = 5;
    MessageStatus status = 6;
    Email message_data = 7;
    // Whether the message is or will be archived.
    bool archive = 8;
    // References to related objects.
    repeated Reference references = 9;
    // RFC 2822 Message-ID
    MessageId message_id = 16;
}

/*
Enum with available reference types.
*/
enum ReferenceType
{
    contact = 0;
    domain = 1;
    nsset = 2;
    keyset = 3;
    registrar = 4;
}

/*
A reference between a message and an object.
*/
message Reference
{
    ReferenceType type = 1;
    // Value is interpreted according to the object type.
    string value = 2;
}



syntax = "proto3";

import "fred_api/messenger/common_types.proto";
import "fred_api/messenger/email.proto";
import "google/protobuf/timestamp.proto";

package Fred.Messenger.Api.Email;

message SendEmailRequest
{
    // Email message to be send.
    Email message_data = 1;
    // Whether the message should be archived. True if not defined.
    oneof Archive {
        bool archive = 2;
    }
    // References to related objects.
    repeated Reference references = 3;
}

message SendEmailReply
{
    Data data = 1;
    message Data
    {
        // Email message unique identifier
        Uid uid = 1;
    }
}

message GetEmailRequest
{
    // Email message unique identifier
    Uid uid = 1;
}

message GetEmailReply
{
    Data data = 1;
    message Data
    {
        EmailEnvelope envelope = 1;
    }
}

message ListEmailRequest
{
    // Lower limit for create datetime
    google.protobuf.Timestamp created_from = 1;
    // Upper limit for create datetime
    google.protobuf.Timestamp created_to = 2;
    // List of recipient email addresses to filter.
    repeated string recipients = 3;
    // List of body template names to filter.
    repeated string body_templates = 4;
    // List of message types to filter.
    repeated string types = 6;
    // List of references to filter.
    // All messages that matches at least one of the reference are returned.
    repeated Reference references = 7;
    // Maximum number of messages returned
    uint64 limit = 5;
}

message ListEmailReply
{
    Data data = 1;
    message Data
    {
        EmailEnvelope envelope = 1;
    }
}

service EmailMessenger
{
    // Send email message
    //
    // * Return INVALID_ARGUMENT if required arguments are missing, or recipient contains multiple addresses.
    // * Return FAILED_PRECONDITION in case of database errors.
    rpc send (SendEmailRequest) returns (SendEmailReply) {}
    // Get email envelope
    //
    // * Return INVALID_ARGUMENT if invalid UID is used.
    // * Return NOT_FOUND if message is not found.
    // * Return FAILED_PRECONDITION in case of database errors.
    rpc get (GetEmailRequest) returns (GetEmailReply) {}
    // List email envelopes filtered by parameters.
    //
    // * Return FAILED_PRECONDITION in case of database errors.
    rpc list (ListEmailRequest) returns (stream ListEmailReply) {}
}

