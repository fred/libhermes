/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libhermes/email/send.hh"

#include "include/libhermes/connection.hh"
#include "include/libhermes/exception.hh"
#include "include/libhermes/service.hh"
#include "include/libhermes/strong_type.hh"

#include "src/email/proto_unwrap.hh"
#include "src/email/proto_wrap.hh"
#include "src/email/send_email_reply.hh"
#include "src/email/send_email_request.hh"
#include "src/get_stub.hh"

#include "fred_api/messenger/service_email_grpc.pb.h"

#include <google/protobuf/util/json_util.h>

#include <grpcpp/client_context.h>

#include <boost/uuid/uuid_io.hpp>

#include <exception>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <utility>

namespace LibHermes {
namespace Email {

namespace Api = Fred::Messenger::Api;

namespace {

std::string serialize(Api::Email::SendEmailRequest& _api_send_email_request)
{
        google::protobuf::util::JsonPrintOptions json_print_options;
        json_print_options.preserve_proto_field_names = true;
        json_print_options.add_whitespace = false;
        std::string json;
        google::protobuf::util::MessageToJsonString(_api_send_email_request, &json, json_print_options);
        return json;
}

void modify_request_chunk_for_one_recipient(SendEmailRequest& _send_email_request, const std::pair<RecipientEmail, std::set<RecipientUuid>>& _recipient)
{
    _send_email_request.message_data.recipients = {_recipient};
}

void merge_recipients_uuids_to_references(SendEmailRequest& _send_email_request)
{
    for (const auto& recipient : _send_email_request.message_data.recipients)
    {
        for (const auto& recipient_uuid : recipient.second)
        {
            _send_email_request.references.insert(Reference{Reference::Type{"contact"}, Reference::Value{boost::uuids::to_string(*recipient_uuid)}});
        }
    }
}

} // namespace LibHermes::Email::{anonymous}

EmailUid send(
        const Connection<Service::EmailMessenger>& _connection,
        const Email& _email,
        const Archive& _archive,
        const std::set<Reference>& _references)
{
    SendEmailRequest send_email_request{_email, _archive, _references};
    merge_recipients_uuids_to_references(send_email_request);
    Api::Email::SendEmailRequest api_send_email_request;
    proto_wrap(send_email_request, &api_send_email_request);

    grpc::ClientContext context;
    const auto timeout = _connection.get_timeout();
    if (timeout != no_explicit_connection_timeout)
    {
        const auto deadline =
                std::chrono::system_clock::now() +
                std::chrono::seconds(timeout);
        context.set_deadline(deadline);
    }
    Api::Email::SendEmailReply send_email_reply;
    const auto status = get_stub(_connection).send(&context, api_send_email_request, &send_email_reply);

    if (!status.ok())
    {
        throw SendFailed(status.error_code(), status.error_message(), serialize(api_send_email_request));
    }

    return proto_unwrap(send_email_reply).uid;
}

std::vector<EmailUid> batch_send(
        const Connection<Service::EmailMessenger>& _connection,
        const Email& _email,
        const Archive& _archive,
        const std::set<Reference>& _references)
{
    std::vector<EmailUid> reply;

    grpc::ClientContext context;

    auto streamer = get_stub(_connection).batch_send(&context);

    std::exception_ptr ex;
    std::thread writer([&]() {
            try
            {
                for (const auto& recipient : _email.recipients)
                {
                    SendEmailRequest send_email_request{_email, _archive, _references};
                    modify_request_chunk_for_one_recipient(send_email_request, recipient);
                    merge_recipients_uuids_to_references(send_email_request);

                    Api::Email::SendEmailRequest api_send_email_request;
                    proto_wrap(send_email_request, &api_send_email_request);
                    const auto stream_ok = streamer->Write(api_send_email_request);
                    if (!stream_ok)
                    {
                        throw Exception{"libhermes: bad gRPC stream"};
                    }
                }
                streamer->WritesDone();
            }
            catch (...)
            {
                ex = std::current_exception();
                return;
            }
    });

    Api::Email::SendEmailReply send_email_reply;
    while (streamer->Read(&send_email_reply))
    {
        reply.push_back(proto_unwrap(send_email_reply).uid);
        send_email_reply.Clear();
    }

    writer.join();
    if (ex != nullptr)
    {
        std::rethrow_exception(ex);
    }
    const auto status = streamer->Finish();
    if (!status.ok())
    {
        SendEmailRequest send_email_request{_email, _archive, _references};
        merge_recipients_uuids_to_references(send_email_request);
        Api::Email::SendEmailRequest api_send_email_request;
        proto_wrap(send_email_request, &api_send_email_request);
        throw SendFailed(status.error_code(), status.error_message(), serialize(api_send_email_request));
    }

    return reply;
}

} // namespace LibHermes::Email
} // namespace LibHermes
