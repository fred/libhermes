/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PROTO_UNWRAP_HH_D57E024FB6694EB9A66EF85C459E857F
#define PROTO_UNWRAP_HH_D57E024FB6694EB9A66EF85C459E857F

#include "src/email/send_email_reply.hh"

#include "fred_api/messenger/service_email_grpc.pb.h"

namespace LibHermes {
namespace Email {

SendEmailReply proto_unwrap(const Fred::Messenger::Api::Email::SendEmailReply& _src);

} // namespace LibHermes::Email
} // namespace LibHermes

#endif
