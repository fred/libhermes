/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_WRAP_HH_B958DE09CA8A4D718F6671D3C1488223
#define PROTO_WRAP_HH_B958DE09CA8A4D718F6671D3C1488223

#include "include/libhermes/email/email.hh"

#include "src/email/send_email_request.hh"

#include "fred_api/messenger/service_email_grpc.pb.h"

namespace LibHermes {
namespace Email {

template <typename>
struct InternalTypeTraits
{
    struct ProtoType;
};

template <>
struct InternalTypeTraits<Email>
{
    using ProtoType = Fred::Messenger::Api::Email::Email;
};

template <>
struct InternalTypeTraits<SendEmailRequest>
{
    using ProtoType = Fred::Messenger::Api::Email::SendEmailRequest;
};

template <typename InternalType>
void proto_wrap(const InternalType& _src, typename InternalTypeTraits<InternalType>::ProtoType* _dst);

template<>
void proto_wrap(const SendEmailRequest& _src, InternalTypeTraits<SendEmailRequest>::ProtoType* _dst);

} // namespace LibHermes::Email
} // namespace LibHermes

#endif
