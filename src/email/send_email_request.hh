/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SEND_EMAIL_REQUEST_HH_5CAA0D34039C48DC9A60009BBC5B3012
#define SEND_EMAIL_REQUEST_HH_5CAA0D34039C48DC9A60009BBC5B3012

#include "include/libhermes/email/email.hh"
#include "include/libhermes/reference.hh"

#include "libstrong/type.hh"

#include <set>

namespace LibHermes {
namespace Email {

struct SendEmailRequest
{
    using Archive = LibStrong::Type<bool, struct ArchiveTag_>;

    Email message_data;
    Archive archive;
    std::set<Reference> references;
};

} // namespace LibHermes::Email
} // namespace LibHermes

#endif
