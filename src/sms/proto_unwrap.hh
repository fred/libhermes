/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_UNWRAP_HH_4C4F97AC51E046DBAFC6DDF81D6107F7
#define PROTO_UNWRAP_HH_4C4F97AC51E046DBAFC6DDF81D6107F7

#include "src/sms/send_sms_reply.hh"

#include "fred_api/messenger/service_sms_grpc.pb.h"

namespace LibHermes {
namespace Sms {

SendSmsReply proto_unwrap(const Fred::Messenger::Api::Sms::SendSmsReply& _src);

} // namespace LibHermes::Sms
} // namespace LibHermes

#endif
