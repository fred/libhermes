/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_WRAP_HH_1C17C37D7C20467C83426FBF2420137C
#define PROTO_WRAP_HH_1C17C37D7C20467C83426FBF2420137C

#include "include/libhermes/sms/message_data.hh"

#include "src/sms/send_sms_request.hh"

#include "fred_api/messenger/service_sms_grpc.pb.h"

namespace LibHermes {
namespace Sms {

template <typename>
struct InternalTypeTraits
{
    struct ProtoType;
};

template <>
struct InternalTypeTraits<MessageData>
{
    using ProtoType = Fred::Messenger::Api::Sms::Sms;
};

template <>
struct InternalTypeTraits<SendSmsRequest>
{
    using ProtoType = Fred::Messenger::Api::Sms::SendSmsRequest;
};

template <typename InternalType>
void proto_wrap(const InternalType& _src, typename InternalTypeTraits<InternalType>::ProtoType* _dst);

template<>
void proto_wrap(const SendSmsRequest& _src, InternalTypeTraits<SendSmsRequest>::ProtoType* _dst);

} // namespace LibHermes::Sms
} // namespace LibHermes

#endif
