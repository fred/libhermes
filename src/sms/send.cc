/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libhermes/sms/send.hh"

#include "include/libhermes/connection.hh"
#include "include/libhermes/exception.hh"
#include "include/libhermes/service.hh"
#include "include/libhermes/strong_type.hh"

#include "src/sms/proto_unwrap.hh"
#include "src/sms/proto_wrap.hh"
#include "src/sms/send_sms_reply.hh"
#include "src/sms/send_sms_request.hh"
#include "src/get_stub.hh"

#include "fred_api/messenger/service_sms_grpc.pb.h"

#include <google/protobuf/util/json_util.h>

#include <grpcpp/client_context.h>

#include <boost/uuid/uuid_io.hpp>

#include <chrono>
#include <iostream>
#include <stdexcept>

namespace LibHermes {
namespace Sms {

namespace Api = Fred::Messenger::Api;

namespace {

std::string serialize(const Api::Sms::SendSmsRequest& _api_send_sms_request)
{
    google::protobuf::util::JsonPrintOptions json_print_options;
    json_print_options.preserve_proto_field_names = true;
    json_print_options.add_whitespace = false;
    std::string json;
    google::protobuf::util::MessageToJsonString(_api_send_sms_request, &json, json_print_options);
    return json;
}

void merge_recipients_uuids_to_references(SendSmsRequest& _send_sms_request)
{
    for (const auto& recipient_uuid : _send_sms_request.message_data.recipient_uuids)
    {
        _send_sms_request.references.insert(Reference{Reference::Type{"contact"}, Reference::Value{boost::uuids::to_string(*recipient_uuid)}});
    }
}

} // namespace LibHermes::Sms::{anonymous}

SmsUid send(
        const Connection<Service::SmsMessenger>& _connection,
        const MessageData& _message_data,
        const Optional<Archive>& _archive,
        const std::set<Reference>& _references)
{
    SendSmsRequest send_sms_request{_message_data, _archive, _references};
    merge_recipients_uuids_to_references(send_sms_request);
    Api::Sms::SendSmsRequest api_send_sms_request;
    proto_wrap(send_sms_request, &api_send_sms_request);

    grpc::ClientContext context;
    const auto timeout = _connection.get_timeout();
    if (timeout != no_explicit_connection_timeout)
    {
        const auto deadline =
                std::chrono::system_clock::now() +
                std::chrono::seconds(timeout);
        context.set_deadline(deadline);
    }
    Api::Sms::SendSmsReply api_send_sms_reply;
    const auto status = get_stub(_connection).send(&context, api_send_sms_request, &api_send_sms_reply);

    if (!status.ok())
    {
        throw SendFailed(status.error_code(), status.error_message(), serialize(api_send_sms_request));
    }

    return proto_unwrap(api_send_sms_reply).uid;
}

} // namespace LibHermes::Sms
} // namespace LibHermes
