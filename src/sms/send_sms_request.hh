/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEND_SMS_REQUEST_HH_365878053C52433E90F756D477DE6FC8
#define SEND_SMS_REQUEST_HH_365878053C52433E90F756D477DE6FC8

#include "include/libhermes/sms/message_data.hh"
#include "include/libhermes/reference.hh"

#include "include/libhermes/strong_type.hh"

#include <set>

namespace LibHermes {
namespace Sms {

struct SendSmsRequest
{
    using Archive = StrongBool<struct ArchiveTag_>;

    MessageData message_data;
    Optional<Archive> archive;
    std::set<Reference> references;
};

} // namespace LibHermes::Sms
} // namespace LibHermes

#endif
