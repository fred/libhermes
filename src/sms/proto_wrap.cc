/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/sms/proto_wrap.hh"

#include "src/proto_wrap.hh"

#include <boost/uuid/uuid_io.hpp>

namespace LibHermes {
namespace Sms {

namespace Api = Fred::Messenger::Api;

template <>
struct InternalTypeTraits<Type>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<BodyTemplate>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<BodyTemplateUuid>
{
    using ProtoType = Fred::Messenger::Api::Uuid;
};

template<>
void proto_wrap(const BodyTemplateUuid& _src, InternalTypeTraits<BodyTemplateUuid>::ProtoType* _dst)
{
    _dst->set_value(boost::uuids::to_string(*_src));
}

template<>
void proto_wrap(const MessageData& _src, InternalTypeTraits<MessageData>::ProtoType* _dst)
{
    _dst->set_recipient(*(_src.recipient_phone_number));
    if (!is_nullopt(_src.type))
    {
        _dst->set_type(**(_src.type));
    }
    _dst->set_body_template(*(_src.body_template));
    proto_wrap(_src.context, _dst->mutable_context());
    if (!is_nullopt(_src.body_template_uuid))
    {
        proto_wrap(*(_src.body_template_uuid), _dst->mutable_body_template_uuid());
    }
}

template<>
void proto_wrap(const SendSmsRequest& _src, InternalTypeTraits<SendSmsRequest>::ProtoType* _dst)
{
    proto_wrap(_src.message_data, _dst->mutable_message_data());

    if (!is_nullopt(_src.archive))
    {
        _dst->set_archive(**(_src.archive));
    }

    _dst->mutable_references()->Reserve(_src.references.size());
    for (const auto& ref : _src.references)
    {
        proto_wrap(ref, _dst->add_references());
    }
}

} // namespace LibHermes::Sms
} // namespace LibHermes
