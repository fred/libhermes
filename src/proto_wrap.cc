/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/proto_wrap.hh"

#include <boost/none_t.hpp>

#include <stdexcept>

namespace LibHermes {

namespace {

void proto_wrap(bool _src, google::protobuf::Value* _dst)
{
    _dst->set_bool_value(_src);
}

void proto_wrap(int _src, google::protobuf::Value* _dst)
{
    double src_as_double = static_cast<double>(_src);
    if (static_cast<int>(src_as_double) != _src)
    {
        throw std::runtime_error("unable to convert integer to double");
    }
    _dst->set_number_value(src_as_double);
}

void proto_wrap(std::string _src, google::protobuf::Value* _dst)
{
    _dst->set_string_value(std::move(_src));
}

void proto_wrap(boost::none_t, google::protobuf::Value* _dst)
{
    _dst->set_null_value(google::protobuf::NULL_VALUE);
}

void proto_wrap(const Struct& _src, google::protobuf::Value* _dst)
{
    proto_wrap(_src, _dst->mutable_struct_value());
}

void proto_wrap(const std::vector<StructValue>& _src, google::protobuf::Value* _dst)
{
    auto dst = new google::protobuf::ListValue;
    dst->mutable_values()->Reserve(_src.size());
    for (const auto& item : _src)
    {
        //proto_wrap(item.value, dst.add_values());
        //           ^^^^^^^^^^ boost::variant
        boost::apply_visitor(
                [&](auto i)
                {
                    proto_wrap(i, dst->add_values());
                },
                item.value);

    }
    _dst->set_allocated_list_value(dst);
}

} // namespace LibHermes::{anonymous}

template<>
void proto_wrap(const Struct& _src, InternalTypeTraits<Struct>::ProtoType* _dst)
{
    for (const auto& item : _src)
    {
        boost::apply_visitor(
                [&](auto&& i)
                {
                    proto_wrap(std::forward<decltype(i)>(i), &(*(_dst->mutable_fields()))[*(item.first)]); // XXX
                },
                item.second.value);
    }
}

template<>
void proto_wrap(const Reference& _src, InternalTypeTraits<Reference>::ProtoType* _dst)
{
    _dst->set_type(*_src.type);
    _dst->set_value(*_src.value);
}


} // namespace LibHermes
